package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Z_Exercise {

    public static void main(String[] args) {

        //ADD EXERCISE ON EVERYTHING


        // they should use InputOutput.java
        // Create file java.exercise.2020.txt in a directory src\\testFolder
        String pathname = "src\\testFolder\\";
        try {
            File newDirectory = new File(pathname);
            if (!newDirectory.exists()) {
                newDirectory.mkdir();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String filename = "java.exercise.2020.txt";
        String fileStr = pathname + filename;
        File file = new File(fileStr);
        try {
            if (!file.isFile()) {
                file.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Write to the file - only once this text
        // put new lines wherever you would like
        String data = "Excellent documentation support - Javadocs\r\n When I first saw Javadoc, I was amazed. It's a great piece of documentation, which tells a lot of things about Java API. I think without Javadoc documentation, Java wouldn't be as popular, and it's one of the main reasons, Why I believe Java is the best programming language.\r\n Not everyone has time and intention to look at the code to learn what a method does or how to use a class. Javadoc made learning easy and provide an excellent reference while coding in Java.\r\n With the advent of IDEs like Eclipse and IntelliJIDEA, you don't even need to look Javadoc explicitly in the browser, but you can get all the information in your IDE window itself.\r\n";
        try {
            FileWriter myWriter = new FileWriter(fileStr);
            myWriter.write(data);
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Read from file, so the text will appear in console
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                System.out.println(line);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // Delete file and print out if it was successful
        if (file.delete()) {
            System.out.println("Successfully deleted file");
        } else {
            System.out.println("Delete was not successful");
        }




    }





}
