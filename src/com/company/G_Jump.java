package com.company;

/*
jump: Java supports three jump statement: break, continue and return.
These three statements transfer control to other part of the program.
 */
public class G_Jump {

    public static void main(String[] args) {

//    Break: In Java, break is majorly used for:
//      Using break, we can force immediate termination of a loop,
//      bypassing the conditional expression
//      and any remaining code in the body of the loop.

    // Initially loop is set to run from 0-9
        for (int i = 0; i < 10; i++) {
        // terminate loop when i is 5.
        if (i == 5)
            break;
        System.out.println("i: " + i);
    }
        System.out.println("Loop complete.");


        System.out.println("");
//    Continue: Sometimes it is useful to force an early iteration of a loop.
//    That is, you might want to continue running the loop
//    but stop processing the remainder of the code in its body for this particular iteration.
        for (int i = 0; i < 10; i++) {
            // If the number is even
            // skip and continue
            if (i % 2 == 0)
                continue;
            // If number is odd, print it
            System.out.print(i + " ");
        }

    }
}


// Return:
// The return statement is used to explicitly return from a method.
// That is, it causes a program control to transfer back to the caller of the method.
class Return {
    public static void main(String args[]) {
        String newWord = returnString();
        System.out.println(newWord);


        boolean bool = true;
        System.out.println("Before the return.");
        if (bool)
            return;
        // Compiler will bypass every statement
        // after return
        System.out.println("This won't execute.");
    }

    public static String returnString() {
        String word = "Hello!";
        return word;
    }
}