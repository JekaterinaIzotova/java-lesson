package com.company;

/*
When executing Java code, different errors can occur:
coding errors made by the programmer,
errors due to wrong input,
or other unforeseeable things.

When an error occurs, Java will normally stop and generate an error message.
The technical term for this is:
Java will throw an exception (throw an error).
 */
public class H_Exception {

    public static void main(String[ ] args) {
        int[] myNumbers = {1, 2, 3};
//        System.out.println(myNumbers[10]); // error!

//    The try statement allows you to define a block of code to be tested for errors while it is being executed.
//    The catch statement allows you to define a block of code to be executed, if an error occurs in the try block.

        try {
            int[] myNumbers2 = {1, 2, 3};
//            System.out.println(myNumbers2[10]);
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            e.printStackTrace();
        }


//    The finally statement lets you execute code, after try...catch, regardless of the result:
        try {
            int[] myNumbers3 = {1, 2, 3};
            System.out.println(myNumbers[10]);
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            e.printStackTrace();
        } finally {
            System.out.println("The 'try catch' is finished.");
        }


    }

}
