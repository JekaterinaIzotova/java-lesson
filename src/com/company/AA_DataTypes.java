package com.company;

/*
There are 8 primitive data types
Primitive data are only single values and have no special capabilities.

The Reference Data Types will contain a memory address of variable value
because the reference types won’t store the variable value directly in memory.
They are strings, objects, arrays, etc.
 */
public class AA_DataTypes {

    public static void main(String[] args) {
// Primitive data types
        boolean boo;
        byte by;
        char ch;
        short sh;
        int in;
        long lo;
        float fl;
        double dou;


        /*
        boolean: either true or false
        default value: false
        */
        boolean booleanVar;
        booleanVar = true;
        if (booleanVar == true) {
            System.out.println("Hello, World");
        }
        if (booleanVar) {
            System.out.println("Hello, World");
        }

        /*
        byte: The byte data type is an 8-bit signed two’s complement integer.
        The byte data type is useful for saving memory in large arrays.
        Values: -128 to 127
        Default value: 0
         */
        byte byteVar = 126;
        byte b = 126;
        // this will give error as number is
        // larger than byte range
//        byte b1 = 7888888955;

        // byte is 8 bit value
        System.out.println("byteVar " + byteVar);

        byteVar++;
        System.out.println("byteVar " + byteVar);

        // It overflows here because
        // byte can hold values from -128 to 127
        byteVar++;
        System.out.println("byteVar " + byteVar);

        // Looping back within the range
        byteVar++;
        System.out.println("byteVar " + byteVar);

        /*
        short: The short data type is a 16-bit signed two’s complement integer.
        Similar to byte, use a short to save memory in large arrays,
        in situations where the memory savings actually matters.
        Values: -32768 to 32767 (inclusive)
        Default: 0
        */
        short shortVar;
        short s = 56;
        // this will give error as number is
        // larger than short range
        // short s1 = 87878787878;

        /*
        int: It is a 32-bit signed two’s complement integer.
        Values: -2147483648 to 2147483647 (inclusive)
        Default: 0
         */
        int intVar;
        int i = 89;

        /*
        long: The long data type is a 64-bit two’s complement integer.
        Values: -9223372036854775808 to 9223372036854775807 (inclusive)
        Default: 0
         */
        long longVar;

        /*
        float: The float data type is a single-precision 32-bit IEEE 754 floating point.
        Use a float (instead of double) if you need to save memory in large arrays of floating point numbers.
        Values: upto 7 decimal digits
        Default: 0.0
         */
        float floatVar;
        // for float use 'f' as suffix
        floatVar = 4.7f;
        float f = 3.432342f;

        /*
        double: The double data type is a double-precision 64-bit IEEE 754 floating point.
        For decimal values, this data type is generally the default choice.
        Values: upto 16 decimal digits
        Default: 0.0
         */
        double doubleVar;
        doubleVar = 4.5;
        double d = 4.23423234;

        /*
        char: The char data type is a single 16-bit Unicode character.
        Values: '\u0000' (0) to '\uffff' (65535)
        Default: '\u0000'
         */
        char charVar;
        char c = 'G';


        System.out.println("");
        System.out.println("char: " + c);
        System.out.println("integer: " + i);
        System.out.println("byte: " + b);
        System.out.println("short: " + s);
        System.out.println("float: " + f);
        System.out.println("double: " + d);




    }
}
