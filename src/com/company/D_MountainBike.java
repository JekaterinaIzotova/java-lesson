/*
    Packages:
        The prefix of a unique package name is always written in all-lowercase ASCII letters
        and should be one of the top-level domain names, like com, edu, gov, mil, net, org.
        Examples:
            com.sun.eng
            com.apple.quicktime.v2
            java.lang
 */
package com.company;

/*
    Class definition:
    This line uses the keyword class to declare that a new class is being defined.
    MountainBike is an identifier that is the name of the class.
    The entire class definition, including all of its members, will be between the curly braces { }.

    Class name should be nouns, in mixed case with the first letter of each internal word capitalised.
    Use whole words and must avoid acronyms and abbreviations.

    FileName: "MountainBike.java".
    In java file name and public class name should be the same
    One file can contain several classes, but only one public
*/
public class D_MountainBike {

    /*
        Variables: variable names should be short yet meaningful.
        Variables can also start with either underscore(‘_’) or dollar sign ‘$’ characters.
        Should be mnemonic i.e, designed to indicate to the casual observer the intent of its use.
        One-character variable names should be avoided except for temporary variables, like
        i, j, k, m, and n for integers;
        c, d, and e for characters
     */
    int speed = 0;
    int gear = 1;

    /*
        Constant variables:
        Should be all uppercase with words separated by underscores (“_”).
        There are various constants used in predefined classes like Float, Long, String etc.
     */
    static final int MAX_GEAR = 21;

    /*
        Methods :
        Methods should be verbs, in mixed case with the first letter lowercase
        and with the first letter of each internal word capitalised.
     */

    void changeGear(int newValue) {};
    void speedUp(int increment) {};
    void applyBrakes(int decrement) {};
}



