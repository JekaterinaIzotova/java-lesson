package com.company;

import java.io.File;
import java.util.ArrayList;

public class I_FileActions {

    public static void main(String[] args) {

        // create a directory if it does not exist
        String filepath = "src\\newDirectory\\";
        boolean isDirCreated = I_InputOutput.createDirectory(filepath);
        System.out.println("Directory created: " + isDirCreated);

        // create a file if it does not exist
        String filename = "myFile.txt";
        boolean isFileCreated = I_InputOutput.createFile(filepath, filename);
        System.out.println("File created: " + isFileCreated);

        // write data to a file
        String fileStr = filepath + filename;
        String dataStr = "Files in Java might be tricky, but it is fun enough!\r\n";
        boolean isWritten = I_InputOutput.writeToFile(fileStr, dataStr);
        System.out.println("Written to file: " + isWritten);

        // read from a file
        ArrayList<String> data = I_InputOutput.readFromFile(fileStr);
        System.out.println(data.toString());
        for (String line : data) {
            System.out.println(line);
        }



        // To get more information about a file, use any of the File methods:
        File file = new File(fileStr);
        if (file.exists()) {
            //apply File class methods on File object
            System.out.println("File name: " + file.getName());
            System.out.println("Path: " + file.getAbsolutePath());
            System.out.println("Absolute path: " + file.getAbsolutePath());
            System.out.println("Writeable: " + file.canWrite());
            System.out.println("Parent:" + file.getParent());
            System.out.println("Readable " + file.canRead());
            System.out.println("File size in bytes " + file.length());
        } else {
            System.out.println("The file does not exist.");
        }



        // delete file
        boolean isDeleted = I_InputOutput.deleteFile(file);
        if (isDeleted) {
            System.out.println("Deleted the file: " + file.getName());
        } else {
            System.out.println("Failed to delete the file.");
        }

    }
}
