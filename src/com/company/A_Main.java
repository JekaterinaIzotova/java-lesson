package com.company;

/*
Every line of code that runs in Java must be inside a class.
A class should always start with an uppercase first letter.
Note: Java is case-sensitive: "main" and "mAin" has different meaning.

The name of the java file must match the class name.
When saving the file, save it using the class name.
*/
public class A_Main {

//    Your program begins with a call to main().
//    The main() method is required and you will see it in every Java program:
    public static void main(String[] args) {
        // we can use the println() method to print a line of text to the screen
        System.out.println("Hello World");
    }
}
