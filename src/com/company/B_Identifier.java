package com.company;

/*
    In Java, an identifier can be a class name, method name, variable name, or label. For example :
    B_Identifier : class name.
    main : method name.
    String : predefined class name.
    args : variable name.
    num : variable name.
 */
public class B_Identifier {

    public static void main(String[] args) {
        int num = 20;
    }
/*
    Allowed characters for identifiers
        alphanumeric characters([A-Z],[a-z],[0-9])
        ‘$‘ (dollar sign)
        ‘_‘ (underscore)

    Identifiers should not start with digits([0-9])
    Java identifiers are case-sensitive.
    Advisable to use an optimum length of 4 – 15 letters only.
    Reserved Words can’t be used as an identifier..
 */
    // VALID
    String MyVariable;
    String MYVARIABLE;
    String myvariable;
    String x;
    String i;
    String x1;
    String i1;
    String _myvariable;
    String $myvariable;
    String sum_of_array;
    String subjects123;

    /* NOT VALID
    String My Variable;  // contains a space
    String 123subjects;   // Begins with a digit
    String a+c; // plus sign is not an alphanumeric character
    String variable-2; // hyphen is not an alphanumeric character
    String sum_&_difference; // ampersand is not an alphanumeric character
    */
}
