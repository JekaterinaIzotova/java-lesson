package com.company;
/*
A variable is only a name given to a memory location,
all the operations done on the variable effects that memory location.
The value stored in a variable can be changed during program execution.
In Java, all the variables must be declared before use.
 */
public class C_Variable {

    public static void main(String[] args) {

        // declare
        // datatype + variable name
        int number;

        // initialize
        number = 12;

        C_Variable obj = new C_Variable();
        obj.StudentAge();
    }

    // Local Variables
    // These variable are created when the block in entered or the function is called
    // and destroyed after exiting from the block or when the call returns from the function.
    public void StudentAge() {
        // local variable age
        int age = 0;
        age = age + 5;
        System.out.println("Student age is: " + age);
    }
}

// Instance Variables
// As instance variables are declared in a class,
// these variables are created when an object of the class is created
// and destroyed when the object is destroyed.
class Marks {
    // These variables are instance variables.
    // These variables are in a class
    // and are not inside any function
    int engMarks;
    int mathsMarks;
    int phyMarks;
}

class MarksDemo {
    public static void main(String args[]) {
        // first object
        Marks obj1 = new Marks();
        obj1.engMarks = 50;
        obj1.mathsMarks = 80;
        obj1.phyMarks = 90;

        // second object
        Marks obj2 = new Marks();
        obj2.engMarks = 80;
        obj2.mathsMarks = 60;
        obj2.phyMarks = 85;

        // displaying marks for first object
        System.out.println("Marks for first object:");
        System.out.println(obj1.engMarks);
        System.out.println(obj1.mathsMarks);
        System.out.println(obj1.phyMarks);

        // displaying marks for second object
        System.out.println("Marks for second object:");
        System.out.println(obj2.engMarks);
        System.out.println(obj2.mathsMarks);
        System.out.println(obj2.phyMarks);
    }
}



//  Static Variables
//  These variables are declared similarly as instance variables,
//  the difference is that static variables are declared using the static keyword
//  within a class outside any method constructor or block.
class Emp {
    // static variable salary
    public static double salary;
    public static String name = "Harsh";
}

class EmpDemo {
    public static void main(String args[]) {
        // accessing static variable without object
        Emp.salary = 1000;
        System.out.println(Emp.name + "'s average salary: " + Emp.salary);
    }
}