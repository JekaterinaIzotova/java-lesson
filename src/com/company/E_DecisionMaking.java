package com.company;
/*
Java’s Selection statements:
    if
    if-else
    if-else-if
    switch-case
    jump – break, continue, return
These statements allow you to control the flow of your program’s execution based upon conditions known only during run time.
 */

public class E_DecisionMaking {
    public static void main(String[] args) {
//      if:
//      It is used to decide whether a certain statement or block of statements
//      will be executed or not i.e if a certain condition is true
//      then a block of statement is executed otherwise not.
        int i = 10;
        if (i > 15)
            System.out.println("10 is less than 15");

        // This statement will be executed
        // as if considers one statement by default
        System.out.println("I am Not in if");


//      if-else:
//      We can use the else statement with if statement
//      to execute a block of code when the condition is false.
        int n = 10;
        if (n < 15)
            System.out.println("n is smaller than 15");
        else
            System.out.println("n is greater than 15");


//      if-else-if:
//      The if statements are executed from the top down.
//      As soon as one of the conditions controlling the if is true,
//      the statement associated with that if is executed,
//      and the rest of the ladder is bypassed.
//      If none of the conditions is true, then the final
//      else statement will be executed.
        int num2 = 20;

        if (num2 == 10)
            System.out.println("i is 10");
        else if (num2 == 15)
            System.out.println("i is 15");
        else if (num2 == 20)
            System.out.println("i is 20");
        else
            System.out.println("i is not present");


//      switch-case:
//      The switch statement is a multiway branch statement.
//      It provides an easy way to dispatch execution to different parts
//      of code based on the value of the expression.
        int nr = 9;
        switch (nr) {
            case 0:
                System.out.println("i is zero.");
                break;
            case 1:
                System.out.println("i is one.");
                break;
            case 2:
                System.out.println("i is two.");
                break;
            default:
                System.out.println("i is greater than 2.");
        }
    }

}
