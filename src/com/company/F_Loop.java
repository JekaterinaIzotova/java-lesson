package com.company;

public class F_Loop {

    public static void main(String args[]) {
//    while loop:
//    A while loop is a control flow statement that allows code
//    to be executed repeatedly based on a given Boolean condition.
//    The while loop can be thought of as a repeating if statement.
        int j = 1;

        // Exit when num becomes greater than 4
        while (j <= 4) {
            System.out.println("Value of j:" + j);
            // Increment the value of j for
            // next iteration
            j++;
        }

//    do while:
//    do while loop is similar to while loop with only difference
//    that it checks for condition after executing the statements
        int num = 21;
        do {
            // The line will be printed even
            // if the condition is false
            System.out.println("Value of num:" + num);
            num++;
        }
        while (num < 20);


//    for loop:
//    Unlike a while loop, a for statement consumes the initialization,
//    condition and increment/decrement in one line
//    thereby providing a shorter, easy to debug structure of looping.

        // for loop begins when num=2
        // and runs till num <=4
        for (int i = 2; i <= 4; i++) {
            System.out.println("Value of i:" + i);
            System.out.println();
        }

//    Enhanced For loop
//    Enhanced for loop provides a simpler way to iterate through the elements of a collection or array
        String array[] = {"Ron", "Harry", "Hermoine"};

        //enhanced for loop
        for (String element : array) {
            System.out.println(element);
        }

        /* for loop for same function
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
        */





    }
}
