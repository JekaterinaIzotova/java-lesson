package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class I_InputOutput {

    /*
    create directory if it does not exist
    */
    public static boolean createDirectory(String pathname) {
        try {
            File newDirectory = new File(pathname);
            if (!newDirectory.exists()) {
                newDirectory.mkdir();
                return true;
            } else {
                System.out.println("Folder '" + pathname + "' already exists");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
    create file if it does not exist
     */
    public static boolean createFile(String filepath, String filename) {
        File myFile = new File(filepath + filename);
        try {
            if (!myFile.isFile()) {
                myFile.createNewFile();
                return true;
            } else {
                System.out.println("File '" + myFile + "' already exists");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
    write to a file
     */
    public static boolean writeToFile(String file, String data) {
        try {
            FileWriter myWriter = new FileWriter(file);
            for (int i = 0; i < 5; i++) {
                myWriter.write(data);
            }
            myWriter.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /*
    read from a file
     */
    public static ArrayList readFromFile(String file) {
        ArrayList<String> data = new ArrayList();
        try {
            File myFile = new File(file);
            Scanner myReader = new Scanner(myFile);
            while (myReader.hasNextLine()) {
                String line = myReader.nextLine();
                data.add(line);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return data;
    }

    /*
    delete a file
     */
    public static boolean deleteFile(File file) {
        if (file.delete()) {
            return true;
        } else {
            return false;
        }
    }





}
